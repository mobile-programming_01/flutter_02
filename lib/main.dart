

import 'package:flutter/material.dart';


enum APP_THEME{LIGHT,DRAK}


void main() {
  runApp(ContactProfilePage());
}


class MyAppTheme{
  //Light lightness
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
            color: Colors.indigo.shade500
        )
    );
  }

  //Light Darkness
  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
            color: Colors.indigo.shade500
        )
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DRAK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),//control dark or light from class static
      home: Scaffold(
         appBar : buildAppBarWidget(),
         body : buildBodyWidget(),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            // icon : Icon(Icons.add),
            onPressed: (){
              setState(() {
                currentTheme == APP_THEME.DRAK

              });
            },
            ),
          ),
      );

  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.textsms,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}
Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}
Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}


//ListTile
Widget mobileListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.deepPurple,
      onPressed: (){},
    ),
  );
}

Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(""),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.deepPurple,
      onPressed: (){},
    ),
  );
}

Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("Priyanka@gmail.com"),
    subtitle: Text("gmail"),
    trailing: IconButton(
      icon: Icon(Icons.email),
      color: Colors.deepPurple,
      onPressed: (){},
    ),
  );
}

Widget addressListTile(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("234 Sunset St. Burlingame"),
    subtitle: Text("address"),
    trailing: IconButton(
      icon: Icon(Icons.directions,),
      color: Colors.deepPurple,
      onPressed: (){},
    ),
  );
}

AppBar buildAppBarWidget(){
  return AppBar(
    backgroundColor: Colors.pinkAccent,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
          onPressed: () {},
          icon: Icon(Icons.star_border),
          color : Colors.black
      )
    ],
  );
}

Widget buildBodyWidget(){
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            //Height constraint at Container widget level
            height: 250,
            child: Image.network(
              "https://allaboutcats.com/wp-content/uploads/2020/10/Ragdoll-Cat-compressed.jpg",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Priyanka Tyagi",
                    style: TextStyle(fontSize:30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),

          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data : ThemeData(
              iconTheme: IconThemeData(
                color: Colors.green
              ),
            ),
              child: profileActionItem(),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          mobileListTile(),
          otherPhoneListTile(),

          Divider(
            color: Colors.grey,
          ),

          emailListTile(),
          Divider(
            color: Colors.grey,
          ),

          addressListTile(),

        ],
      ),
    ],
  );
}

Widget profileActionItem() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}